package com.bank.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.dto.AccountDetailsDTO;
import com.bank.dto.AccountRegDTO;
import com.bank.dto.FundTransferDTO;
import com.bank.dto.FundTransferSuccessDTO;
import com.bank.entity.Account;
import com.bank.entity.Customer;
import com.bank.entity.CustomerCard;
import com.bank.repo.AccountRepo;
import com.bank.repo.CustomerCardRepo;
import com.bank.repo.CustomerRepo;





@Service
public class BankServiceImpl implements BankService{
	@Autowired
	CustomerRepo customerRepo;

	@Autowired
	AccountRepo accountRepo;

	@Autowired
	CustomerCardRepo customerCardRepo;

	@Override
	public FundTransferSuccessDTO fundTransfer(FundTransferDTO fundTransferDTO) {

		CustomerCard customerCard = verifyAccount(fundTransferDTO.getCustomerCardNo());
		CustomerCard vendorCard = verifyAccount(fundTransferDTO.getVendorCardNo());

		if (Objects.isNull(customerCard) || Objects.isNull(vendorCard))
			return FundTransferSuccessDTO.builder().message("Account Doesn't exist").build();

		if (!(customerCard.getCardNo().equals(fundTransferDTO.getCustomerCardNo())
				&& customerCard.getCvv().equals(fundTransferDTO.getCvv())
				&& customerCard.getExpriryDate().equals(fundTransferDTO.getExpriryDate()))) {
			return FundTransferSuccessDTO.builder().message("card details doesn't match").build();
		}

		Customer fromCustomer = customerRepo.findByCustomerCard(customerCard);

		Optional<Account> fromAccount = accountRepo.findByCustomer(fromCustomer).stream()
				.max(Comparator.comparing(Account::getId));

		double fromAccountRemBalance = BigDecimal.valueOf(fromAccount.get().getBalance())
				.subtract(BigDecimal.valueOf(fundTransferDTO.getAmount())).doubleValue();

		if (fromAccountRemBalance < 0) {
			return FundTransferSuccessDTO.builder().message("Not enough balance in your account").build();
		}

		long transactionId = System.currentTimeMillis();

		fromAccount = Optional.ofNullable(Account.builder().debit(fundTransferDTO.getAmount()).credit(Double.valueOf(0))
				.balance(fromAccountRemBalance).transactionId(transactionId).remarks(fundTransferDTO.getRemarks())
				.customer(fromCustomer).build());

		accountRepo.save(fromAccount.get());

		Customer toCustomer = customerRepo.findByCustomerCard(vendorCard);

		Optional<Account> toAccountToSave = accountRepo.findByCustomer(toCustomer).stream()
				.max(Comparator.comparing(Account::getId));

		double toAccountRemBalance = BigDecimal.valueOf(toAccountToSave.get().getBalance())
				.add(BigDecimal.valueOf(fundTransferDTO.getAmount())).doubleValue();

		toAccountToSave = Optional.ofNullable(Account.builder().credit(fundTransferDTO.getAmount())
				.debit(Double.valueOf(0)).balance(toAccountRemBalance).transactionId(transactionId)
				.remarks(fundTransferDTO.getRemarks()).customer(toCustomer).build());

		accountRepo.save(toAccountToSave.get());

		return FundTransferSuccessDTO.builder().message("Transaction success").transactionId(transactionId)
				.statusCode("200").build();
	}

	@Override
	public AccountDetailsDTO register(AccountRegDTO accountRegDTO) {

		String cardNo = getCardNo(16);
		String cvv = getCardNo(3);
		LocalDate expiry = LocalDate.now().plusDays(365 * 10);

		CustomerCard customerCard = customerCardRepo
				.save(CustomerCard.builder().cardNo(cardNo).cvv(cvv).expriryDate(expiry).build());

		long transactionId = System.currentTimeMillis();

		Customer customer = Customer.builder().firstName(accountRegDTO.getFirstName())
				.lastName(accountRegDTO.getLastName()).address(accountRegDTO.getAddress())
				.gender(accountRegDTO.getGender()).dob(accountRegDTO.getDob()).pan(accountRegDTO.getPan())
				.aadhar(accountRegDTO.getAadhar()).contactNo(accountRegDTO.getContactNo()).customerCard(customerCard)
				.build();

		customer = customerRepo.save(customer);

		Account account = Account.builder().debit(Double.valueOf(0)).credit(Double.valueOf(10000))
				.balance(Double.valueOf(10000)).transactionId(transactionId).remarks("new account open")
				.customer(customer).build();

		accountRepo.save(account);

		return AccountDetailsDTO.builder().cardNo(cardNo).cvv(cvv).expriryDate(expiry)
				.message("account registration success").build();
	}

	public static String getCardNo(int n) {
		StringBuilder s = new StringBuilder("0123456789");
		StringBuilder cardNo = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			int index = (int) (s.length() * Math.random());
			cardNo.append(s.charAt(index));
		}
		return cardNo.toString();
	}

	@Override
	public CustomerCard verifyAccount(String customerCardNo) {
		return customerCardRepo.findByCardNo(customerCardNo);
	}

	@Override
	public List<TransactionHistoryDTO> txHistory(String contactNo) {
		Customer customer = customerRepo.findByContactNo(contactNo);
		List<Account> accounts = accountRepo.findByCustomer(customer);
		List<TransactionHistoryDTO> txListDTO = new ArrayList<>(accounts.size());
		for (Account a : accounts) {
			TransactionHistoryDTO t = new TransactionHistoryDTO();
			BeanUtils.copyProperties(a, t);
			txListDTO.add(t);
		}
		return txListDTO;
	}


}
