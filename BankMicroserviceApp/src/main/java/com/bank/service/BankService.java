package com.bank.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bank.dto.AccountDetailsDTO;
import com.bank.dto.AccountRegDTO;
import com.bank.dto.FundTransferDTO;
import com.bank.dto.FundTransferSuccessDTO;
import com.bank.dto.TransactionHistoryDTO;
import com.bank.entity.CustomerCard;



@Service
public interface BankService {
	
	FundTransferSuccessDTO fundTransfer(FundTransferDTO fundTransferDTO);
	AccountDetailsDTO register(AccountRegDTO accountRegDTO);
	CustomerCard verifyAccount(String contactNo);
	List<TransactionHistoryDTO> txHistory(String contactNo);
	
}
