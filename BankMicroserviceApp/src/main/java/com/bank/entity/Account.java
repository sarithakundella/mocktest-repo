package com.bank.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "debit")
	private Double debit;

	@Column(name = "credit")
	private Double credit;
	
	@Column(name="balance")
	private Double balance;
	
	@Column(name = "transaction_id")
	private Long transactionId;

	@Column(name = "remarks")
	private String remarks;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="customer_id", referencedColumnName = "id")
	private Customer customer;

}
