package com.bank.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "customer")
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "address")
	private String address;

	@Column(name = "gender")
	private Character gender;

	@Column(name = "dob")
	private LocalDate dob;

	@Column(name = "pan")
	private String pan;

	@Column(name = "aadhar")
	private String aadhar;

	@Column(name = "contact_no", unique = true)
	private String contactNo;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="card_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "customer_customerCard_fk"))
	private CustomerCard customerCard;

}
