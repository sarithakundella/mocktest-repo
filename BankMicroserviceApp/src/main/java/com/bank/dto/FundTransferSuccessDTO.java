package com.bank.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FundTransferSuccessDTO {

	private long transactionId;
	private String message;
	private String statusCode;

}
