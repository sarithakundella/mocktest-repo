package com.bank.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.bank.entity.CustomerCard;

@Service
public interface CustomerCardRepo extends JpaRepository<CustomerCard, Integer>{
	
	public CustomerCard findByCardNo(String cardNo);

	
}
