package com.bank.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.bank.entity.Customer;
import com.bank.entity.CustomerCard;

@Service
public interface CustomerRepo extends JpaRepository<Customer, Integer>{

	public Customer findByCustomerCard(CustomerCard customerCard);
	
	public Customer findByContactNo(String contactNo);
	
}
