package com.bank.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.bank.entity.Account;
import com.bank.entity.Customer;

@Service
public interface AccountRepo extends JpaRepository<Account, Integer>{

	public List<Account> findByCustomer(Customer customer);
	
}
