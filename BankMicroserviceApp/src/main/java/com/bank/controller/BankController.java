package com.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dto.AccountDetailsDTO;
import com.bank.dto.AccountRegDTO;
import com.bank.dto.FundTransferDTO;
import com.bank.dto.FundTransferSuccessDTO;
import com.bank.service.BankService;
import com.bank.service.TransactionHistoryDTO;

@RestController
@RequestMapping("/customer")
public class BankController {

	@Autowired
	BankService bankService;

	@PostMapping("/register")
	public ResponseEntity<AccountDetailsDTO> register(@RequestBody AccountRegDTO accountRegDTO) {
		return new ResponseEntity<>(bankService.register(accountRegDTO), HttpStatus.OK);
	}

	@PostMapping("/fundTransfer")
	public ResponseEntity<FundTransferSuccessDTO> fundTransfer(@RequestBody FundTransferDTO fundTransferDTO) {
		return new ResponseEntity<>(bankService.fundTransfer(fundTransferDTO), HttpStatus.OK);
	}

	@GetMapping("/txHistory")
	public ResponseEntity<List<TransactionHistoryDTO>> transactionHistory(@RequestParam String contactNo) {
		return new ResponseEntity<>(bankService.txHistory(contactNo), HttpStatus.OK);
	}
}
