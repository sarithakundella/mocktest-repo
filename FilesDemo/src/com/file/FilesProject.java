package com.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class FilesProject {

	public static void main(String[] args) throws Exception {

		File folder = new File("C:\\Users\\JavaPrograms");
		File[] files = folder.listFiles();
		for (File file : files) {
			if (file.isFile()) {
				System.out.println("file name is:" + file);
				String line, word = "";
				int count = 0, maxCount = 0;
				ArrayList<String> words = new ArrayList<String>();

				FileReader fr = new FileReader("file ");
				BufferedReader br = new BufferedReader(fr);

				while ((line = br.readLine()) != null) {
					String string[] = line.toLowerCase().split("");
					// Adding all words generated in previous step into words
					for (String s : string) {
						words.add(s);
					}
				}

				for (int i = 0; i < words.size(); i++) {
					count = 1;

					for (int j = i + 1; j < words.size(); j++) {
						if (words.get(i).equals(words.get(j))) {
							count++;
						}
					}

					if (count > maxCount) {
						maxCount = count;
						word = words.get(i);
					}
				}

				System.out.println("Most repeated word: " + word);
				br.close();
			}
		}
	}
}
