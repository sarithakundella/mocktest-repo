package com.shopping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@Enable
public class ShoppingCartMicroserviceAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingCartMicroserviceAppApplication.class, args);
	}

}
