package com.shopping.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.shopping.model.Order;

@Service
public interface OrderRepo extends JpaRepository<Order, Integer>{

	List<Order> findByCustomerContactNo(String customerContactNo);

}
