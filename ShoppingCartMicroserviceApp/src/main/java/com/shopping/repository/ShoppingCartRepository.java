package com.shopping.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shopping.model.ShoppingCart;
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, Long> {
	//@Query("select * from product where staus=?")
    List<ShoppingCart> findByStatus(String status);
}
