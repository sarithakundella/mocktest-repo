package com.shopping.repository;

import org.springframework.data.repository.CrudRepository;

import com.shopping.model.User;


public interface UserRepository extends CrudRepository<User, Long> {
}
