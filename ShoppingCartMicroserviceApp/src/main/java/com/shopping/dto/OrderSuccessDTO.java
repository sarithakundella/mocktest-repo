package com.shopping.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderSuccessDTO {
	
	private long orderId;
	private String message;
	private String itemName;
	private int quantity;
	private double price;

}
