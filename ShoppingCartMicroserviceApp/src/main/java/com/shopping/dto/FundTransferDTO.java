package com.shopping.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FundTransferDTO {

	@NotNull
	private String customerCardNo;
	@NotNull
	private String cvv;
	@NotNull
	private LocalDate expriryDate;
	@NotNull
	private String vendorCardNo;
	@NotNull
	private double amount;
	@NotNull
	private String remarks;
	
		

	
}
