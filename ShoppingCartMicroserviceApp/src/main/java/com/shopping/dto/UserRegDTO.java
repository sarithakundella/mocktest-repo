package com.shopping.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRegDTO {

	@NotNull
	private String firstName;
	private String lastName;
	@NotNull
	private String address;
	@NotNull
	private Character gender;
	@NotNull
	private LocalDate dob;
	@NotNull
	private String pan;
	@NotNull
	private String aadhar;
	@NotNull
	private String contactNo;

}
