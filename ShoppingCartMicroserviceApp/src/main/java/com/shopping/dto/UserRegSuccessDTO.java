package com.shopping.dto;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserRegSuccessDTO {

	private long userId;
	private String message;

}
