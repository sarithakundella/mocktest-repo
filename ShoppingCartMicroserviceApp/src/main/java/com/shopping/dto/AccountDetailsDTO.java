package com.shopping.dto;


import java.time.LocalDate;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AccountDetailsDTO {

	private String message;
	private String cardNo;
	private String cvv;
	private LocalDate expriryDate;

}
