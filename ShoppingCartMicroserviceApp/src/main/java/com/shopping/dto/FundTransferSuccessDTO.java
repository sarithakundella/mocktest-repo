package com.shopping.dto;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FundTransferSuccessDTO {

	private String transactionId;
	private String message;
	private String statusCode;

}
