package com.shopping.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopping.FeinService.BankService;
import com.shopping.dto.FundTransferDTO;
import com.shopping.dto.FundTransferSuccessDTO;
import com.shopping.dto.OrderSuccessDTO;
import com.shopping.dto.OrderSuccessDTOs;
import com.shopping.dto.UserRegDTO;
import com.shopping.dto.UserRegSuccessDTO;
import com.shopping.model.Order;
import com.shopping.model.Product;
import com.shopping.repository.OrderRepo;
import com.shopping.repository.ProductRepository;
import com.shopping.repository.UserRepository;
@Service
public class OrderServiceimpl {
	
	
	@Autowired
	UserRepository userRepo;

	@Autowired
	ProductRepository productRepo;

	@Autowired
	BankService bankService;

	@Autowired
	OrderRepo orderRepo;

	@Override
	public UserRegSuccessDTO register(UserRegDTO userRegDTO) {

		long userId = System.currentTimeMillis();

		productRepo.save(userRegDTO.builder().firstName(userRegDTO.getFirstName()).lastName(userRegDTO.getLastName())
				.address(userRegDTO.getAddress()).gender(userRegDTO.getGender()).dob(userRegDTO.getDob())
				.pan(userRegDTO.getPan()).aadhar(userRegDTO.getAadhar()).contactNo(userRegDTO.getContactNo())
				.userId(userId).build());

		return UserRegSuccessDTO.builder().userId(userId).message("Registration Success").build();
	}

	

	public OrderSuccessDTOs order(OrdersDTO ordersDTO) {
		
		BigDecimal totalAmount = BigDecimal.valueOf(0);

		for (OrderDTO o : ordersDTO.getOrderDTO()) {
			Optional<Product> menu=menuRepo.findById(o.getMenuId());
			totalAmount = BigDecimal.valueOf(menu.get().getPrice()).add(totalAmount);
		}

		FundTransferSuccessDTO fundTransferSuccessDTO = bankService
				.order(FundTransferDTO.builder().customerCardNo(ordersDTO.getCustomerCardNo()).cvv(ordersDTO.getCvv())
						.expriryDate(ordersDTO.getExpriryDate()).vendorCardNo(ordersDTO.getVendorCardNo())
						.amount(totalAmount.doubleValue()).remarks(ordersDTO.getRemarks()).build());

		Order order = null;

		List<OrderSuccessDTO> orderSuccessDTOList = Collections.emptyList();
		
		if (fundTransferSuccessDTO.getStatusCode().equals("200")) {

			orderSuccessDTOList = new ArrayList<>(ordersDTO.getOrderDTO().size());

			for (OrderDTO o : ordersDTO.getOrderDTO()) {

				Optional<Product> menu = menuRepo.findById(o.getMenuId());

				order = orderRepo.save(Order.builder().status("Order Placed")
						.transactionId(fundTransferSuccessDTO.getTransactionId()).menu(menu.get())
						.vendorCardNo(ordersDTO.getVendorCardNo()).customerCardNo(ordersDTO.getCustomerCardNo())
						.customerContactNo(ordersDTO.getCustomerContactNo()).build());

				OrderSuccessDTO orderSuccessDTO = OrderSuccessDTO.builder().orderId(order.getId())
						.message("Order Placed").itemName(menu.get().getItemName()).quantity(1)
						.price(menu.get().getPrice()).build();

				orderSuccessDTOList.add(orderSuccessDTO);
			}
		}
		return OrderSuccessDTOs.builder().orderSuccessDTO(orderSuccessDTOList).build();
	}

}
