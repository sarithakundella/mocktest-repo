package com.shopping.model;

import java.awt.Menu;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Builder;
import lombok.Data;

@Entity
     
@Data
@Builder

	public class Order {

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@Column(name = "id")
		private Integer id;

		@Column(name = "status")
		private String status;

		@Column(name = "transaction_id")
		private String transactionId;

		@Column(name = "vendor_card_no")
		private String vendorCardNo;

		@Column(name = "customer_card_no")
		private String customerCardNo;

		@Column(name = "customer_contact_no")
		private String customerContactNo;

		

		

	}


