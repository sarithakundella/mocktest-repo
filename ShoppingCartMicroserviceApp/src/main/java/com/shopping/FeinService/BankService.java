package com.shopping.FeinService;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.shopping.RibbonConfig;
import com.shopping.dto.AccountDetailsDTO;
import com.shopping.dto.FundTransferDTO;
import com.shopping.dto.FundTransferSuccessDTO;





@Service
@FeignClient(name="bankmicroservice", configuration = RibbonConfig.class)
public interface BankService {
	@PostMapping("/fundTransfer")
	FundTransferSuccessDTO fundTransfer(@RequestBody FundTransferDTO fundTransferDTO);
	//AccountDetailsDTO register(AccountRegDTO accountRegDTO);
	//CustomerCard verifyAccount(String contactNo);
	//List<TransactionHistoryDTO> txHistory(String contactNo);
	
}
